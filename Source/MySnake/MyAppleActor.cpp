// Fill out your copyright notice in the Description page of Project Settings.

#include "MySnake.h"
#include "MyAppleActor.h"
#include "MySnakeActor.h"
#include "Components/SphereComponent.h"

// Sets default values
AMyAppleActor::AMyAppleActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyRootComponent = CreateDefaultSubobject<USphereComponent>("RootEeat");
	RootComponent = MyRootComponent;

	SnakeEeatMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>
		(TEXT("/Engine/BasicShapes/Sphere")).Object;

    UMaterialInstance* EatColor;

	EatColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>
		(TEXT("MaterialInstanceConstant'/Game/SnakeContent/Materials/InstansMaterials/M_Metal_Gold_Inst.M_Metal_Gold_Inst'")).Get();

	FVector Size = FVector(0.5f, 0.5f, 0.5f);

	class UStaticMeshComponent* EeatChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Eeat"));

	EeatChank->SetStaticMesh(SnakeEeatMesh);
	EeatChank->SetRelativeScale3D(Size);
	EeatChank->SetRelativeLocation(FVector(0, 0, 0));
	EeatChank->SetMaterial(0,EatColor);
	EeatChank->AttachTo(MyRootComponent);
	EeatChank->SetSimulatePhysics(true);
}

// Called when the game starts or when spawned
void AMyAppleActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyAppleActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	BuferTime += DeltaTime;
	if (BuferTime > StepDeley)
	{
		Destroy(true, true);
	}
	CollectEat();
}

void AMyAppleActor::CollectEat()
{
	//TArray<AActor*> CollectedActors;
	GetOverlappingActors(CollectedActors);

	for (int32 i = 0; i < CollectedActors.Num(); ++i)
	{
		AMySnakeActor* const Test = Cast<AMySnakeActor>(CollectedActors[i]);
		if (Test)
		{
			Test->VisibleBodyChank++;
			Test->score++;
			Destroy(true, true);
			break;
		}
	}
}

