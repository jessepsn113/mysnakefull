// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPawnCamera.h"
#include "MySnake.h"
#include "MySnakeActor.h"
#include "Camera/CameraComponent.h"
#include "Components/BoxComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "MyAppleActor.h"

// Sets default values
AMyPawnCamera::AMyPawnCamera()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyRootComponent = CreateDefaultSubobject<UBoxComponent>("RooModel");
	RootComponent = MyRootComponent;
	CameraSpring = CreateDefaultSubobject<USpringArmComponent>("Spring");
	CameraSpring->SetRelativeLocation(FVector(0,0,0));
	CameraSpring->AttachTo(MyRootComponent);
	MyCamera = CreateDefaultSubobject<UCameraComponent>("camera");
	MyCamera->AttachTo(CameraSpring, USpringArmComponent::SocketName);

	CameraSpring->SetRelativeRotation(FRotator(-90.f,0,0));
	CameraSpring->TargetArmLength = 1700.f;
	CameraSpring->bDoCollisionTest = false;

	AutoPossessPlayer = EAutoReceiveInput::Player0;

}

// Called when the game starts or when spawned
void AMyPawnCamera::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void AMyPawnCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (GameMode > 0)
	{
		BuferTime += DeltaTime;
		if (BuferTime>StepDeley)
		{
			if (!GamePause) AddRandomApple();
			BuferTime = 0;
		}
	}
}

// Called to bind functionality to input
void AMyPawnCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("KeyMapMove", this, &AMyPawnCamera::FMove);
}

void AMyPawnCamera::AddSnakeToMap()
{
	FVector StartPoint = GetActorLocation();
	FRotator StartPointRotation = GetActorRotation();

	if (GetWorld())
	{
		MySnakePlayer = GetWorld()->SpawnActor<AMySnakeActor>(StartPoint, StartPointRotation);
		GameMode = 1;
		MySnakePlayer->WhoPawn = this;
	}
	
}

void AMyPawnCamera::FMove(float ButtonVal)
{
	int32 Key = ButtonVal;
	if (GameMode > 0)
	{
		if (Key == 5)
		{
			GamePause = !GamePause;
		}
		if (!GamePause)
		{
			switch (Key)
			{
			case 1:
				if (WSAD.X !=1)
				{
					WSAD = FVector2D(0,0);
					WSAD.X = -1;
				}
				break;
			case 2:
				if (WSAD.X !=-1)
				{
					WSAD = FVector2D(0,0);
					WSAD.X = 1;
				}
				break;
			case 3:
				if (WSAD.Y !=-1)
				{
					WSAD = FVector2D(0,0);
					WSAD.Y = 1;
				}
				break;
			case 4:
				if (WSAD.Y !=1)
				{
					WSAD = FVector2D(0,0);
					WSAD.Y = -1;
				}
				break;
			}

			if (MySnakePlayer)
			{
				MySnakePlayer->DirectionMoveSnake = WSAD;
			}
		}
		else
		{
			MySnakePlayer->DirectionMoveSnake = FVector2D(0,0);
		}
	}
}

void AMyPawnCamera::AddRandomApple()
{
	FRotator StartPointRotation = FRotator (0,0,0);
	float SpawnX = FMath::FRandRange(MinX,MaxX);
	float SpawnY = FMath::FRandRange(MinY,MaxY);

	FVector StartPoint = FVector(SpawnX,SpawnY,SpawnZ);

	if (MySnakePlayer)
	{
		if (GetWorld())
		{
			GetWorld()->SpawnActor<AMyAppleActor>(StartPoint, StartPointRotation);
		}
	}
}

int32 AMyPawnCamera::GetScore()
{
	if (MySnakePlayer)
	{
		return MySnakePlayer->score;
	}
	return 0;
}

void AMyPawnCamera::SnakeDestroy()
{
	GameMode = 0;
	if (MySnakePlayer) MySnakePlayer->Destroy(true,true);
}